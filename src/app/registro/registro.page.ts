import { Component, OnInit, Input } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { AppComponent } from '../app.component';
import { AuthenticationService } from '../services/Authentication.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  @Input() texto = 'Cargando...';
  @Input() sms = '¡Campos incompletos!';
  @Input() pass = '¡Contraseñas diferentes!';
  @Input() credenciales = '¡Credenciales incorrectas!';
  @Input() usuario_inex = '¡Usuario inexistente!';
  @Input() sms_correo = '¡Correo Incorrecto!';
  loader = false;
  alertas_ = false;
  alertas_p = false;
  alertas_c = false;
  alertas_u = false;
  alerta_correo = false;

  inicio = true;
  registro_login = false;
  welcome = false;
  register = false;

  typepass = 'password';
  registro = {
    nombre: '',
    apellido: '',
    mail: '',
    password: ''
  }
  confimar_pass;
  login = {
    mail: '',
    password: ''
  }
  expresion = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

  constructor(private menu: MenuController, private screen: ScreenOrientation, private navCtrl: NavController,
    private appCom: AppComponent, private authService: AuthenticationService) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
    this.screen.onChange().subscribe(
      () => {  });
  }

  ngOnInit() {
    this.menu.enable(false);
  }
  registro_normal() {
    if (this.registro.nombre === null || this.registro.nombre === '' || this.registro.nombre === undefined || this.registro.apellido === null || this.registro.apellido === '' || this.registro.apellido === undefined ||
      this.registro.mail === null || this.registro.mail === '' || this.registro.mail === undefined || this.registro.password === null || this.registro.password === '' || this.registro.password === undefined) {
      this.campos_Incompletos();
    } else {
      if(!this.expresion.test(this.registro.mail)){
        this.alerta_correo = true;
        setTimeout(() => {
          this.alerta_correo = false;
          this.sms_correo = '¡Correo Incorrecto!';
        }, 1000);
      }else{
        if (this.registro.password === this.confimar_pass) {
          this.loader = true;
          setTimeout(() => {
            this.loader = false;
            this.texto = 'Cargando...';
            localStorage.setItem('registro_usuario', JSON.stringify(this.registro));
            this.authService.login();
            this.menu.enable(true);
            this.registro.nombre = null;
            this.registro.apellido = null;
            this.registro.mail = null;
            this.registro.password = null;
            this.confimar_pass = null;
            this.appCom.usuario_header();
          }, 2500);
        } else {
          this.alertas_p = true;
          setTimeout(() => {
            this.pass = '¡Contraseñas diferentes!';
            this.alertas_p = false;
          }, 1000);
        }
      }
    }
  }
//Chats (https://www.youtube.com/watch?v=5Ltug2phHog)
//https://www.youtube.com/watch?v=MGjRiinm67o
  usuario_logueado;
  iniciar_sesion() {
    if (this.login.mail === null || this.login.mail === '' || this.login.mail === undefined ||
      this.login.password === null || this.login.password === '' || this.login.password === undefined) {
      this.campos_Incompletos();
    } else {
      if(!this.expresion.test(this.login.mail)){
        this.alerta_correo = true;
        setTimeout(() => {
          this.alerta_correo = false;
          this.sms_correo = '¡Correo Incorrecto!';
        }, 1000);
      }else{
        if (localStorage.getItem('registro_usuario') === null) {
          this.alertas_u = true;
          setTimeout(() => {
            this.alertas_u = false;
            this.usuario_inex = '¡Usuario inexistente!';
          }, 1000);
        } else {
          this.usuario_logueado = localStorage.getItem('registro_usuario');
          const user_log = JSON.parse(this.usuario_logueado);
          if (this.login.mail === user_log.mail && this.login.password === user_log.password) {
            this.loader = true;
            setTimeout(() => {
              this.loader = false;
              this.texto = 'Cargando...';
              this.authService.login();
              this.appCom.usuario_header();
              this.menu.enable(true);
              this.login.mail = null;
              this.login.password = null;
            }, 2500);
          } else {
            this.alertas_c = true;
            setTimeout(() => {
              this.loader = false;
              this.alertas_c = false;
              this.credenciales = '¡Credenciales incorrectas!';
            }, 1000);
          }
        }
      }
    }
  }

  pantallas(){
    this.inicio = true;
    this.welcome = false;
    this.register = false;
  }
  campos_Incompletos(){
    this.alertas_ = true;
    setTimeout(() => {
      this.sms = '¡Campos Incompletos!';
      this.alertas_ = false;
    }, 1000);
  }
  opciones() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.inicio = false;
      this.registro_login = true;
    }, 2200);
  }
  entrar() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.welcome = true;
      this.registro_login = false;
    }, 2200);
  }
  registrar() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.register = true;
      this.registro_login = false;
    }, 2200);
  }
  entrar_inicio(){
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.welcome = true;
      this.register = false;
      this.login.mail = null;
      this.login.password = null;
    }, 2200);
  }
  entrar_registro(){
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.welcome = false;
      this.register = true;
      this.registro.nombre = null;
      this.registro.apellido = null;
      this.registro.mail = null;
      this.registro.password = null;
      this.confimar_pass = null;
    }, 2200);
  }

}

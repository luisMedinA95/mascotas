import { Component, OnInit } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {

  constructor(private screen: ScreenOrientation, private navCtrl: NavController, private menu: MenuController) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
    this.screen.onChange().subscribe(
      () => { });
   }

  ngOnInit() {
    this.menu.enable(false);
  }
  back(){
    this.navCtrl.navigateForward('/folder/Inbox');
    this.menu.enable(true);
  }

}

import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  opcion = [
    {
      id_opcion: '1',
      opcion_: 'Tomar foto'
    },
    {
      id_opcion: '2',
      opcion_: 'Seleccionar foto'
    },
    {
      id_opcion: '3',
      opcion_: 'Eliminar foto'
    }
  ]

  constructor(private popoverController: PopoverController) { }

  ngOnInit() {
  }

  onClick(val) {
    console.log("Valor:", val);
    this.popoverController.dismiss({
      op: val
    })
  }

}

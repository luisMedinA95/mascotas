import { Injectable } from '@angular/core';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastController: ToastController,
    private alertCtrl: AlertController,
    private loadingController: LoadingController) { }

  myToast: any;

  show_Toast(sms) {
    this.myToast = this.toastController.create({
      message: sms,
      position: 'bottom',
      duration: 1000
    }).then((toastData) => {
      console.log(toastData);
      toastData.present();
    });
  }

  //Loading con opciones
  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      spinner: null,
      duration: 5000,
      message: 'Click the backdrop to dismiss early...',
      translucent: true,
      //cssClass: 'custom-class custom-loading',
      backdropDismiss: true
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed with role:', role);
  }


  //Modal Aceptar y Cancelar
  async Alerta(sms) {
    const alert = await this.alertCtrl.create({
      message: sms,
      //cssClass: 'alert_',
      buttons: [{
        text: 'Aceptar',
        handler: () => {

        }
      }, {
        text: 'Cancelar',
        handler: () => {

        }
      }]
    });
    alert.present();
  }
}

import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AnimationController, AlertController, NavController, MenuController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';


@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.page.html',
  styleUrls: ['./favoritos.page.scss'],
})
export class FavoritosPage implements OnInit {
  animales;
  valor;
  sin_favoritos = false;
  constructor(private actionSheetController: ActionSheetController, private screen: ScreenOrientation,
    private animationCtrl: AnimationController, private alertCtrl: AlertController, private navCtrl: NavController, private menu: MenuController) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
    this.screen.onChange().subscribe(
      () => {  });
    this.valor = localStorage.getItem('val_favoritos');
    this.animales = JSON.parse(this.valor);
    const activo = localStorage.getItem('Identif');
    if(this.animales === null || activo === '1'){
      this.sin_favoritos = true;
      if(activo === '1' && this.animales.indexOf(0)){
        this.sin_favoritos = false;
        if (Object.entries(this.animales).length === 0 && activo === '1'){
          this.sin_favoritos = true;
        }
      }
    }else{
      this.sin_favoritos = false;
    }
    
  }

  ngOnInit() {
    this.menu.enable(false);
  }

  elemento(p) {
    swal({
      text: "¿Estas seguro de eliminar de favoritos?",
      icon: "warning",
      dangerMode: true,
    })
    .then(eliminado => {
      if (eliminado) {
        var index = this.animales.indexOf(p);
          if (index > -1) {
            this.animales.splice(index, 1);
            localStorage.setItem('val_favoritos', JSON.stringify(this.animales));
            swal(p.nombre,"Eliminado correctamente de favoritos", "success" );
            if (Object.entries(this.animales).length === 0) {
              console.log("VACIO");
              this.sin_favoritos = true;
              let ident = '1';
            localStorage.setItem('Identif', ident);
            }else{
              this.sin_favoritos = false;
            }
          }
      }
    });
  }

  async Alerta(p) {
    const alerta = await this.alertCtrl.create({
      message: '¿Seguro que deseas eliminarlo de favoritos?',
      buttons: [{
        text: 'Aceptar',
        handler: () => {}
      }, {
        text: 'Cancelar',
        handler: () => { }
      }]
    });
    alerta.present();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      //header: 'Albums',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Eliminar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
        }
      }, {
        text: 'Favorito',
        icon: 'heart',
        handler: () => {
          console.log('Favorite clicked');
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  back(){
    this.navCtrl.navigateForward('/folder/Inbox');
    this.menu.enable(true);
  }

}

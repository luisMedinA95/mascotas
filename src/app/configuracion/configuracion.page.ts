import { Component, OnInit, Input } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { MenuController, NavController, PopoverController } from '@ionic/angular';
import { PopoverPage } from '../popover/popover.page';
import { AuthenticationService } from '../services/Authentication.service';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.page.html',
  styleUrls: ['./configuracion.page.scss'],
})
export class ConfiguracionPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  editperfil = true;
  spiner = false;
  user = true;
  constructor(private screen: ScreenOrientation, private menu: MenuController, private navCtrl: NavController,
    private popoverController: PopoverController, private authenticationService: AuthenticationService) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
    this.screen.onChange().subscribe(
      () => { });
   }

  ngOnInit() {
    this.menu.enable(false);
  }
  async opciones(ev) {
    const popover = await this.popoverController.create({
      component: PopoverPage,
      //cssClass: 'my-custom-class',
      event: ev,
    });
    await popover.present();
    const { data } = await popover.onDidDismiss();
    console.log("-->: ", data.op.id_opcion);
    if (data.op.id_opcion === '1') {
      console.log("1");
    } else if (data.op.id_opcion === '2') {
      console.log("2");
    } else if (data.op.id_opcion === '3') {
      console.log("3");
    }
    console.log("Recivo: ", data);
  }
  close_application(){
    this.authenticationService.logout();
  }
  back(){
    this.navCtrl.navigateForward('/folder/Inbox');
    this.menu.enable(true);
  }

}

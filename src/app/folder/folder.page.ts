import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetController, MenuController, LoadingController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { AppComponent } from '../app.component';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  animales = [
    {
      mascotaId: '1',
      nombre: 'Cae',
      color: 'Negro',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/8759d2539720f678d7b7302594f2a95fo.jpg',
      edad: '6 meses',
      ubicacion: 'San Isidro Boxipe',
      contacto: '5615548358',
      nombrePropietario: 'Luis Medina',
    },
    {
      mascotaId: '2',
      nombre: 'Frida',
      color: 'Blanco',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/5adfe7858c9299299c6eb962a71274a5o.jpg',
      edad: '2 años',
      ubicacion: 'San Felipe',
      contacto: '5615548358',
      nombrePropietario: 'Angel Davila',
    },
    {
      mascotaId: '3',
      nombre: 'Rocky',
      color: 'Gris',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/fb78315239e567a5dba66bc2bc78f5b2o.jpg',
      edad: '4 años',
      ubicacion: 'San Juan de las Manzanas',
      contacto: '5615548358',
      nombrePropietario: 'Ines Davila',
    },
    {
      mascotaId: '4',
      nombre: 'Micaela',
      color: 'Gris y Blanco',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/d21259c7f89c74c860f0ad9a7925a997o.jpg',
      edad: '1 meses',
      ubicacion: 'Ixtlahuaca',
      contacto: '5615548358',
      nombrePropietario: 'Luis Enrique Medina Mauro',
    },
    {
      mascotaId: '5',
      nombre: 'Peque',
      color: 'Negro',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/15571f9fd1c16751b3a099bb48aa7912o.jpg',
      edad: '6 meses',
      ubicacion: 'San Isidro Boxipe',
      contacto: '5615548358',
      nombrePropietario: 'Luis Medina',
    },
    {
      mascotaId: '6',
      nombre: 'Coffe',
      color: 'Blanco',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/726912a6fe8fd603e3cddeb4509f94d6o.jpg',
      edad: '2 años',
      ubicacion: 'San Felipe',
      contacto: '5615548358',
      nombrePropietario: 'Angel Davila',
    },
    {
      mascotaId: '7',
      nombre: 'Rocka',
      color: 'Gris',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/8a455e51d99cf475d1eede9b01089d8bo.png',
      edad: '4 años',
      ubicacion: 'San Juan de las Manzanas',
      contacto: '5615548358',
      nombrePropietario: 'Ines Davila',
    },
    {
      mascotaId: '8',
      nombre: 'Camil',
      color: 'Gris',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/687dcc56bbc06de6ac70d81144c8c37eo.jpg',
      edad: '1 mes',
      ubicacion: 'Ixtlahuaca',
      contacto: '5615548358',
      nombrePropietario: 'Emmanuel Garcia',
    },
    {
      mascotaId: '9',
      nombre: 'Kimmie',
      color: 'Negro',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/c5a9edff2146659b73d25e453d977ad2o.jpg',
      edad: '6 meses',
      ubicacion: 'San Isidro Boxipe',
      contacto: '5615548358',
      nombrePropietario: 'Luis Medina',
    },
    {
      mascotaId: '10',
      nombre: 'Dante',
      color: 'Blanco',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/c7bc9202a3f30da8dbb6dcb74a2c45dao.jpg',
      edad: '2 años',
      ubicacion: 'San Felipe',
      contacto: '5615548358',
      nombrePropietario: 'Angel Davila',
    },
    {
      mascotaId: '11',
      nombre: 'Palomo',
      color: 'Gris',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/213bb3598b6816fc4ff948bcfb56f704o.jpg',
      edad: '4 años',
      ubicacion: 'San Juan de las Manzanas',
      contacto: '5615548358',
      nombrePropietario: 'Ines Davila',
    },
    {
      mascotaId: '12',
      nombre: 'Maya',
      color: 'Gris',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/37ff5bc756738bc9f47fab03390ed23co.jpg',
      edad: '1 mes',
      ubicacion: 'Ixtlahuaca',
      contacto: '5615548358',
      nombrePropietario: 'Emmanuel Garcia',
    },
    {
      mascotaId: '13',
      nombre: 'Lau',
      color: 'Negro',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/73a7e5bbac3b3c1824f8b25fd2acd70fo.jpg',
      edad: '6 meses',
      ubicacion: 'San Isidro Boxipe',
      contacto: '5615548358',
      nombrePropietario: 'Luis Medina',
    },
    {
      mascotaId: '14',
      nombre: 'Titi',
      color: 'Blanco',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/9e1656621cef335b80a05c1f09b0300eo.jpg',
      edad: '2 años',
      ubicacion: 'San Felipe',
      contacto: '5615548358',
      nombrePropietario: 'Angel Davila',
    },
    {
      mascotaId: '15',
      nombre: 'Duque',
      color: 'Gris',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/92515c5593e5c4f7a193a8e8957e0399o.jpg',
      edad: '4 años',
      ubicacion: 'San Juan de las Manzanas',
      contacto: '5615548358',
      nombrePropietario: 'Ines Davila',
    },
    {
      mascotaId: '16',
      nombre: 'Andy',
      color: 'Gris',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/8f332a75311cdcb92580a7011eeeabbao.jpg',
      edad: '1 mes',
      ubicacion: 'Ixtlahuaca',
      contacto: '5615548358',
      nombrePropietario: 'Emmanuel Garcia',
    },
    {
      mascotaId: '17',
      nombre: 'coque',
      color: 'Blanco',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/685839b2581c2ac86db31a211a430465o.png',
      edad: '2 años',
      ubicacion: 'San Felipe',
      contacto: '5615548358',
      nombrePropietario: 'Angel Davila',
    },
    {
      mascotaId: '18',
      nombre: 'dany',
      color: 'Gris',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/b10a68d42fc85795df08b7db61c9eefco.jpg',
      edad: '4 años',
      ubicacion: 'San Juan de las Manzanas',
      contacto: '5615548358',
      nombrePropietario: 'Ines Davila',
    },
    {
      mascotaId: '19',
      nombre: 'Matias',
      color: 'Gris',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/ad20285d1d9eb0b1d97c4e20aca280f3o.jpg',
      edad: '1 mes',
      ubicacion: 'Ixtlahuaca',
      contacto: '5615548358',
      nombrePropietario: 'Emmanuel Garcia',
    },
    {
      mascotaId: '20',
      nombre: 'Napoleon',
      color: 'Gris',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/37ff5bc756738bc9f47fab03390ed23co.jpg',
      edad: '1 mes',
      ubicacion: 'Ixtlahuaca',
      contacto: '5615548358',
      nombrePropietario: 'Emmanuel Garcia',
    },
    {
      mascotaId: '21',
      nombre: 'Tachis',
      color: 'Negro',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/73a7e5bbac3b3c1824f8b25fd2acd70fo.jpg',
      edad: '6 meses',
      ubicacion: 'San Isidro Boxipe',
      contacto: '5615548358',
      nombrePropietario: 'Luis Medina',
    },
    {
      mascotaId: '22',
      nombre: 'Yin',
      color: 'Blanco',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/9e1656621cef335b80a05c1f09b0300eo.jpg',
      edad: '2 años',
      ubicacion: 'San Felipe',
      contacto: '5615548358',
      nombrePropietario: 'Angel Davila',
    },
    {
      mascotaId: '23',
      nombre: 'Any',
      color: 'Gris',
      tipoMascota: 'Perro',
      fotoMascota: 'https://fotos.subefotos.com/92515c5593e5c4f7a193a8e8957e0399o.jpg',
      edad: '4 años',
      ubicacion: 'San Juan de las Manzanas',
      contacto: '5615548358',
      nombrePropietario: 'Ines Davila',
    },
    {
      mascotaId: '24',
      nombre: 'Tachis',
      color: 'Gris',
      tipoMascota: 'Gato',
      fotoMascota: 'https://fotos.subefotos.com/8f332a75311cdcb92580a7011eeeabbao.jpg',
      edad: '1 mes',
      ubicacion: 'Ixtlahuaca',
      contacto: '5615548358',
      nombrePropietario: 'Emmanuel Garcia',
    },
  ];
  descripcion = false;
  mascota = {
    mascotaId: null,
    nombre: '',
    color: '',
    tipoMascota: '',
    fotoMascota: '',
    edad: '',
    ubicacion: '',
    contacto: '',
    nombrePropietario: '',
  }
  mis_favoritos = [];
  mis_fav_aux = [];
  favoritos_actualizados = [];
  favoritos_actualizados_aux = [];
  constructor(private actionSheetController: ActionSheetController,
    private menu: MenuController, private screen: ScreenOrientation, private loadingController: LoadingController, 
    private appC: AppComponent, private toastCtrl: ToastService) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
    this.screen.onChange().subscribe(
      () => { });
      const val_us = localStorage.getItem('registro_usuario');
    if(val_us === null){
      console.log("No hay datos que mostrar");
    }else{
      this.appC.usuario_header();
    }
  }

  ngOnInit() {
    this.menu.enable(true);
    this.descripcion = false;
    this.show_Loader();
  }

  valores(val) {
    this.present_Loading(val);
  }

  favorito(val) {
    const upd_fav = localStorage.getItem('update_favoritos');
    const id_up = localStorage.getItem('Identif');
    if (upd_fav === null) {
      if (id_up === '1') {
        const v = localStorage.getItem('val_favoritos');
        this.favoritos_actualizados = JSON.parse(v);
        this.favoritos_actualizados.push(val);

        this.favoritos_actualizados_aux = this.favoritos_actualizados.filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)) === indiceActual
        });

        let sms = '¡Agregado a favoritos!';
        this.toastCtrl.show_Toast(sms);
        localStorage.setItem('val_favoritos', JSON.stringify(this.favoritos_actualizados_aux));
      } else {
        this.mis_favoritos.push(val);
        this.mis_fav_aux = this.mis_favoritos.filter((valorActual, indiceActual, arreglo) => {
          return arreglo.findIndex(valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)) === indiceActual
        });

        let sms = '¡Agregado a favoritos!';
        this.toastCtrl.show_Toast(sms);
        localStorage.setItem('val_favoritos', JSON.stringify(this.mis_fav_aux));
      }
    }
  }

  async present_Loading(val) {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Cargando...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');

    this.descripcion = true;
      this.mascota = val;
  }
  show_Loader(){
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
    }, 500);
  }
  aceptar() {
    this.descripcion = false;
    this.mascota = null;
  }
  cancelar() {
    this.descripcion = false;
    this.mascota = null;
  }
}

import { Component, OnInit, Input, Output } from '@angular/core';
import { Platform, NavController, MenuController, PopoverController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PopoverPage } from './popover/popover.page';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/Authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  public Icns = [
    {
      title: 'Inicio',
      url: '/folder/Inbox',
      icon: 'home'
    }
  ];

  constructor(private platform: Platform, private splashScreen: SplashScreen,
    private statusBar: StatusBar, private screen: ScreenOrientation, 
    private navCtrl: NavController, private camera: Camera, private menu: MenuController,
    private popoverController: PopoverController, 
    private authenticationService: AuthenticationService) {

    this.initializeApp();
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT);
    this.screen.onChange().subscribe(
      () => {  });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.show();
      this.splashScreen.hide();

      this.authenticationService.authState.subscribe(state => {
        if (state) {
          this.navCtrl.navigateForward('/folder/Inbox');
          this.menu.enable(true);
        } else {
          this.navCtrl.navigateForward('/registro');
          this.menu.enable(false);
        }
      });
    });
  }

  nombre: string = null;
  correo: string = null;
  ngOnInit() {
  }
  usuario_header(){
    console.log("Llamado de nuevo");
    const usuario = JSON.parse(localStorage.getItem('registro_usuario'));
    this.nombre = usuario.nombre;
    this.correo = usuario.mail;
  }

  spiner = false;
  user = true;
  imagen_base: string = null;
  imagen_aux: string = null;
  getPicture() {
    this.spiner = true;
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture(options)
      .then(imageData => {
        this.imagen_aux = `data:image/jpeg;base64,${imageData}`;
        //console.log("Subir Imagen real: ",this.imagen_aux);

        localStorage.setItem('tomar_foto', this.imagen_aux);
        this.imagen_base = localStorage.getItem('tomar_foto');
        //this.imagen_base = `data:image/jpeg;base64,${imageData}`;
        this.user = false;
        this.spiner = false;
      })
      .catch(error => {
        this.spiner = false;
        console.error("Error: ", error);
      });
  }
 

  subirPicture() {
    this.spiner = true;
    let optionsGallery: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(optionsGallery).then((imageData) => {
      this.imagen_aux = 'data:image/jpeg;base64,' + imageData;
      //console.log("Subir Imagen real: ",this.imagen_aux);
      localStorage.setItem('subir_foto', this.imagen_aux);
      this.imagen_base = localStorage.getItem('subir_foto');
      //this.imagen_base = 'data:image/jpeg;base64,' + imageData;
      this.user = false;
      this.spiner = false;
    }, (err) => {
      this.spiner = false;
      console.log("Error: ", err);
    })
  }

  // this.aux_ = this.aux.split("base64,")[1];
  //this.imagen_base = 'data:image/jpeg;base64,' + this.aux_;

  async opciones(ev) {
    const popover = await this.popoverController.create({
      component: PopoverPage,
      //cssClass: 'my-custom-class',
      event: ev,
    });
    await popover.present();
    const { data } = await popover.onDidDismiss();
    console.log("-->: ", data.op.id_opcion);
    if (data.op.id_opcion === '1') {
      this.getPicture();
    } else if (data.op.id_opcion === '2') {
      this.subirPicture();
    } else if (data.op.id_opcion === '3') {
      this.eliminar_foto();
    }
    console.log("Recivo: ", data);
  }

  eliminar_foto() {
    this.spiner = true;
      localStorage.removeItem('subir_foto');
      localStorage.removeItem('tomar_foto');
      setTimeout(() => {
        this.imagen_base = null;
        this.spiner = false;
        this.user = true;
      }, 1000);
  }

  inicio() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.navCtrl.navigateForward('/folder/Inbox');
    }, 500);
  }
  favoritos() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.navCtrl.navigateForward('/favoritos');
    }, 500);
  }
  chats() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.navCtrl.navigateForward('/chats');
    }, 500);
  }
  configuracion() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.navCtrl.navigateForward('/configuracion');
    }, 500);
  }

  cerrar_sesion() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.authenticationService.logout();
      this.menu.enable(false);
    }, 1500);
  }
}
